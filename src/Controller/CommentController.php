<?php


namespace App\Controller;


use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends  AbstractController
{

    /**
     * @Route("comments/{id}/vote/{direction<up|down>}", methods="POST")
     */
    public function commentVote($id, $direction, LoggerInterface $logger)
    {
        if($direction === 'up'){
            $currentVoteCount = rand(7, 100);
            $logger->info('Voting Up!!');
        }else{
            $logger->info('Voting Down!!');
            $currentVoteCount = rand(0, 6);
        }

        return $this->json(['votes'=> $currentVoteCount]);
    }
}